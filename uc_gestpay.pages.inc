<?php

/**
 * @file
 * gestpay menu items.
 *
 */

//include "GestPayCrypt.inc.php";

function uc_gestpay_complete($cart_id = 0) {
	_uc_process_response(FALSE);
}

function uc_gestpay_server() {
	_uc_process_response(TRUE);
}

function uc_gestpay_finalize() {
  if (!$_SESSION['do_complete']) {
    drupal_goto('cart');
  }
  $order = uc_order_load(arg(3));
  // Add a comment to let sales team know this came in through the site.
  uc_order_comment_save($order->order_id, 0, t('Order created through website.'), 'admin');
  $output = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));

  $page = variable_get('uc_cart_checkout_complete_page', '');
  if (!empty($page)) {
    drupal_goto($page);
  }
  //$output = uc_order_status_data($order->order_status, 'state');
  return $output;
}


