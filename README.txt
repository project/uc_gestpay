DESCRIPTION: 
This is a module for integration between Ubercart 2.x and Gestpaty Payment
Geteway by Banca Sella (http://www.easynolo.it)

HOW TO USE IT:
1. put the extracted dir uc_gestpay in sites/all/modules/ubercart/payment/
2. Go to admin/build/modules group "Ubercart - payment" to enable it
2. Go to admin/store/settings/payment/edit/methods to enable it in UC
3. Scroll page down to "Gestpay (Banca Sella - easynolo.it) settings"
4. Enter "Vendor ID number", Enable/Disbale Demo Mode
5. Choose language, currency and set the checkout messages

More features will be provided soon.
For info molinari@redmood.it